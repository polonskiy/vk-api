<?php

namespace Vk;

class Api {

    private $token;
    private $curl;

    public function __construct($access_token) {
        $this->token = $access_token;
        $this->curl = curl_init();
    }

    public function __call($method, $args) {
        $method = str_replace('_', '.', $method);
        $args = http_build_query($args[0] + ['access_token' => $this->token]);
        return $this->exec([
            CURLOPT_URL => "https://api.vk.com/method/$method?$args",
        ]);
    }

    public function upload($url, $file, $type = 'photo') {
        $file = is_resource($file) ? stream_get_meta_data($file)['uri'] : $file;
        return $this->exec([
            CURLOPT_URL => $url,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => [$type => new \CURLFile($file)],
        ]);
    }

    private function exec($options = []) {
        curl_reset($this->curl);
        $options += [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_USERAGENT => 'VkApi (https://bitbucket.org/polonskiy/vk-api)',
        ];
        curl_setopt_array($this->curl, $options);
        $response = curl_exec($this->curl);
        if ($response === false) throw new Exception(curl_error($this->curl));
        return json_decode($response, true);
    }

}
