# Vk-Api

Vkontakte API wrapper

## Usage

```
#!php

<?php

$access_token = 'your_access_token';
$gid = 'your_group_id';
$file = '/some/file.jpg';

$vk = new Vk\Api($access_token);
$data = $vk->photos_getWallUploadServer(compact('gid'));
$data = $vk->upload($data['response']['upload_url'], $file);
$data = $vk->photos_saveWallPhoto($data + compact('gid'));
$data = $vk->wall_post(array(
	'owner_id' => "-$gid",
	'attachments' => $data['response'][0]['id'],
	'from_group' => 1,
));
```
